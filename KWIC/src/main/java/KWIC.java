import java.util.*;

import static org.junit.Assert.assertTrue;

class KWIC {

    public static ArrayList<String> printOut(ArrayList<String> Input){
        ArrayList<String> ans = new ArrayList<>();
        ArrayList<String> sortedKey = getKeys(Input);
        ArrayList<String> titles = getContent(Input);

        for(int i = 0;i<sortedKey.size();i++){
            String key = sortedKey.get(i);
            for(int j = 0;j<titles.size();j++){
                String Line = titles.get(j);
                if(Line.contains(key)){
                    int count = 0;
                    for (int pos = Line.indexOf(key); pos >= 0; pos = Line.indexOf(key, pos + 1)){
                        count++;}
                    while(count>=1){
                        int index = Line.indexOf(key);
                        Line = Line.toLowerCase();
                        Line = Line.replaceFirst(Line.substring(index,index+key.length()),key.toUpperCase());
                        ans.add(Line);
                        count--;
                        if(count>=1){
                            int index2 = Line.indexOf(key.toLowerCase());
                            Line = Line.toLowerCase();
                            Line = Line.replace(Line.substring(index2,index2+key.length()),key.toUpperCase());
                            Line = Line.replaceFirst(Line.substring(index,index+key.length()),key.toLowerCase());
                            ans.add(Line);
                            count--;
                        }
                    }
                }
            }
        }
        //System.out.println(ans);
        return ans;
    }
    public static ArrayList<String> getKeys(ArrayList<String> Input){
        ArrayList<String> keys = new ArrayList<String>();
        ArrayList<String> temp = getContent(Input);
        ArrayList<String> ignore = getIgnoreWords(Input);
        int flag = 0;
        for(int i = 0;i<temp.size();i++){
            String line = temp.get(i);
            for(String retval: line.split(" ")){
                keys.add(retval);
            }
        }

        for(int i=0;i<ignore.size();i++){
            String b=ignore.get(i);
            keys.removeIf(s -> s.equalsIgnoreCase(b));
        }
        LinkedHashSet<String> hashSet = new LinkedHashSet<>(keys);
        ArrayList<String> listWithoutDuplicates = new ArrayList<>(hashSet);
        String str;
        for(int i = 0;i<listWithoutDuplicates.size();i++){
            for(int j =i +1;j<listWithoutDuplicates.size();j++){
                String base = listWithoutDuplicates.get(i);
                if(base.compareToIgnoreCase(listWithoutDuplicates.get(j))>0){
                    str = base;
                    listWithoutDuplicates.set(i,listWithoutDuplicates.get(j));
                    listWithoutDuplicates.set(j,str);

                }
            }

        }
        //Collections.sort(listWithoutDuplicates);
        //System.out.println(listWithoutDuplicates);
        return listWithoutDuplicates;
    }

     public static ArrayList<String> getContent(ArrayList<String> Input){
        ArrayList<String> ans = new ArrayList<String>();
         int i = 0;
         while(Input.get(i)!="::"){
             i++;
         }
         for( i = i+1;i<Input.size();i++){
             ans.add(Input.get(i));
         }
        return ans;
      }


       public static ArrayList<String> getIgnoreWords(ArrayList<String> Input){
        ArrayList<String> ans = new ArrayList<String>();
        int i = 0;
       while(Input.get(i)!="::"){
            ans.add(Input.get(i));
            i++;
       }
          return ans;

        }


         public static void main(String[] args)
        {   ArrayList<String> list = new ArrayList<>();
            Scanner in = new Scanner(System.in);
            while(in.hasNextLine()){
                String s = in.nextLine();
                if(s.equals("")){
                     break;}
                list.add(s);
            }
            in.close();

          //test0: read input from stdin and store


            //System.out.println(list);
        }


}





